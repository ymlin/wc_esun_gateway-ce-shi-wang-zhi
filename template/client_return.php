﻿<?php get_header(); ?>


<?php

$order_id=$ORDERNUMBER;
$order=new WC_Order("$order_id");

echo $LTT;
echo "\n";
echo $RRN;
echo "\n";
echo $AUTHCODE;
echo "\n";
$items=$order->get_items();
//echo $order_id.$AUTHSTATUS.$_GET['MID'];


$callback_status=$_POST['rtnCode']; 
$choose_payment=get_post_meta("$order_id",'_choosed_payment',true);
$payment_names=array(
    'credit'=>'無分期',
    'credit_3'=>'分3期',
    'credit_6'=>'分6期'
);
?>
<h1 class='info_title'>授權付款結果</h1>
<?php if($AUTHSTATUS!="00"):?>
<div>信用卡授權失敗。</div>
<div>狀態代碼：<?php echo $order_id;?></div>
<div>狀態訊息：<?php echo get_post_meta($order_id,'_error_msg',true);?></div>
<?php elseif($AUTHSTATUS=="00"):?>
<table class='credit_info info_table'>
    <tr>
        <th>付款方式</th>
        <td><?php echo $payment_names["$choose_payment"];?></td>
    </tr>
    <tr>
        <th>授權狀態</th>
        <td>授權成功</td>
    </tr>
    <tr>
        <th>授權碼</th>
        <td><?php echo get_post_meta("$order_id",'_authcode',true);?></td>
    </tr>
    <tr>
        <th>授權時間</th>
        <td><?php echo get_post_meta("$order_id",'_authtime',true);?></td>
    </tr>
</table>
<?php endif;?>
<h1 class='info_title'>訂單資訊</h1>
<table class='order_info info_table'>
    <tr>
        <th>訂單編號</th>
        <td><a href='account/view-order/<?php echo $order_id;?>/' target='_blank'><?php echo $order_id;?></a></td>
    </tr>
    <tr>
        <th>訂單狀態</th>
        <td><?php echo wc_get_order_status_name($order->get_status());?></td>
    </tr>
    <tr>
        <th>運送方式</th>
        <td><?php echo $order->get_shipping_method();?></td>
    </tr>
    <tr>
        <th>付款方式</th>
        <td><?php echo $order->payment_method_title;?></td>
    </tr>
</table>
<table class='order_products_info info_table'>
    <tr>
        <th>商品</th>
        <th>數量</th>
        <th>小計</th>
    </tr>
    <?php foreach($items as $item): ?>
    <tr>
        <td><a href='<?php echo get_permalink($item['product_id']); ?>' target='_blank'><?php echo $item['name']; ?></a></td>
        <td><?php echo $item['qty']; ?></td>
        <td><?php echo get_woocommerce_currency_symbol();
        echo $item['line_subtotal']+$item['line_subtotal_tax']; ?></td>
    </tr>
    <?php endforeach;?>
    <?php if(($order->order_shipping+$order->order_shipping_tax)!=0): ?>
    <tr>
        <td colspan='2'>運費</td>
        <td><?php echo get_woocommerce_currency_symbol();;
        echo $order->order_shipping+$order->order_shipping_tax;?></td>
    </tr>
    <?php endif; ?>
    <?php if(($order->cart_discount+$order->cart_discount_tax)!=0): ?>
    <tr>
        <td colspan='2'>折扣</td>
        <td>-<?php echo get_woocommerce_currency_symbol();;
        echo $order->cart_discount+$order->cart_discount_tax;?></td>
    </tr>
    <?php endif; ?>
    <tr>
        <td colspan='2'>總計</td>
        <td><?php echo get_woocommerce_currency_symbol();
        echo $order->get_total(); ?></td>
    </tr>
</table>

<h1 class='info_title'>帳單資訊</h1>
<table class='billing_info info_table'>
    <tr>
        <th>姓名</th>
        <td><?php echo $order->billing_last_name.$order->billing_first_name;?></td>
    </tr>
    <tr>
        <th>地址</th>
        <td><?php echo $order->billing_address_1.' '.$order->billing_address_2;?></td>
    </tr>
</table>
<h1 class='info_title'>運送資訊</h1>
<table class='shipping_info info_table'>
    <tr>
        <th>姓名</th>
        <td><?php echo $order->shipping_last_name.$order->shipping_first_name;?></td>
    </tr>
    <tr>
        <th>地址</th>
        <td><?php echo $order->shipping_address_1.' '.$order->shipping_address_2;?></td>
    </tr>
</table>


<?php get_footer(); ?>