<?php

//Plugin Name: WC ESUN Gateway
	
class ESUN_Gateway_Plugin{
    public static $instance;
    public function __construct(){
        include_once('class/WC_ESUN_Gateway.php');
        $this->hooks();
    }
    private function hooks(){
        //加入金流到woocomerce
        add_filter('woocommerce_payment_gateways',array($this,'add_payment_gateway'));
        //註冊wc-api，以接收銀行後端後端post回傳的資料，網址為site_url().'/?wc-api=notify_return';
        add_action('woocommerce_api_'.'notify_return',array($this,'notify_return'));
        
        //加入頁面，接收金流前台回來的回傳資料，以顯示給顧客看
        //頁面網址為site_url().'/?demo_client_return=1'
        //加入wp query參數
        add_filter('query_vars'     , array($this,'query_vars'));
        //加入該query參數的回應方式
    	add_action('parse_request' , array($this,'parse_request'));
    }
    //加入金流到woocomerce
    public function add_payment_gateway($gateways){
        //填入金流ID
        $gateways[]='WC_ESUN_Gateway';
        return $gateways;
    }
    //加入wp query參數demo_client_return
    public function query_vars( $query_vars ){
        $query_vars[] = 'demo_client_return';
        return $query_vars;
    }
    //加入該query參數的回應方式
    public function parse_request( &$wp ){
        if ( array_key_exists( 'demo_client_return', $wp->query_vars ) ) {
            //前台回來的接收頁面
            include 'template/client_return.php';
            exit();
        }
        return;
    }
    //初始化外掛用
    public static function get_instance(){
        if(is_null(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }
    //接收銀行後端回傳的資料，判斷交易是否成功，以修改訂單狀態或是加上備註
    public function notify_return(){
    global $woocommerce;
	$DATA=$_GET['DATA'];
	$DATAstr=array();
	
	$DATAstr=mb_split(",",$DATA);

	$AUTHSTATUS=str_replace ("RC=","",$DATAstr[0]);
	$STOREID=str_replace ("MID=","",$DATAstr[1]);
	$ORDERNUMBER=str_replace ("ONO=","",$DATAstr[2]);
	$AUTHTIME=str_replace ("LTD=","",$DATAstr[3]);
	$LTT=str_replace ("LTT=","",$DATAstr[4]);
	$RRN=str_replace ("RRN=","",$DATAstr[5]);
	$AUTHCODE=str_replace ("AIR=","",$DATAstr[6]);
	

        $order_id=$ORDERNUMBER;
	
        $order=new WC_Order("$order_id");
        //使用$_POST取得資料，以下為範例(依照該金流文件)
      
        if($AUTHSTATUS=="00"){

            //更改訂單狀態為處理中
            $order->update_status('processing');
            //加上訂單備註
            $order->add_order_note('付款完成!',true);
            //減少庫存
            $order->reduce_order_stock();
            //清空購物車
            $woocommerce->cart->empty_cart();
            //紀錄信用卡交易資訊到資料庫
           add_post_meta("$order_id",'_authcode',"$AUTHCODE");
            add_post_meta("$order_id",'_authtime',"$AUTHTIME");
        }else {
		
            $order->add_order_note('付款失敗!',true);
            //紀錄錯誤代碼及訊息到資料庫
           add_post_meta("$order_id",'_error_code',"$AUTHCODE");
            add_post_meta("$order_id",'_error_msg',"$AUTHSTATUS");
        }
		
		 include 'template/client_return.php';
        exit;
       
    
    }
}
add_action('plugins_loaded',array('ESUN_Gateway_Plugin','get_instance'));
?>