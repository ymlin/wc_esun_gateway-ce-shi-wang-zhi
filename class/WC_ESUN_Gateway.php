<?php
class WC_ESUN_Gateway extends WC_Payment_Gateway{
        private $payment_names;
        private $TEST_HOST="https://acqtest.esunbank.com.tw/ACQTrans/esuncard/txnf014s ";//測試用網址
        private $HOST="https://acqtest.esunbank.com.tw/ACQTrans/esuncard/txnf014s ";//正式用網址
        //要傳送過去的資料
        private $send;
        public function __construct(){
            //以下為必要
            
            //金流的Unique ID，自取
            $this->id='WC_ESUN_Gateway';
            //控制結帳欄位的說明欄位是否顯示
            $this->has_fields=true;
            //後台的金流標標題
            $this->method_title='玉山銀行金流';
            //後台的金流說明
            $this->method_description='玉山銀行金流系統';
            //初始化後台設定頁面html欄位
            $this->init_form_fields();
            //初始化後台設定頁面的設定值
            $this->init_settings();
            //讓後台設定頁面可以修改
            add_action('woocommerce_update_options_payment_gateways_'.$this->id,array($this,'process_admin_options'));
            //結帳頁面顯示的標題，由設定讀取
            $this->title=$this->get_option('title');
            
            //以下為非必要
            
            //結帳頁金流圖示
            $this->icon=plugins_url('asset/img/icon.png',__FILE__); 
            //結帳頁金流說明
            $this->description=$this->get_option('description');
            //將自動送出的form，於thank you page顯示出來，以達成自動在付款後導到金流系統
            add_action('woocommerce_thankyou_'.$this->id,array($this,'thankyou_page'));
            //傳送過去的資料定義
            $this->send=array(
    			'MerchantID'=>'',
    			'TerminalID'=>'',
    			'OrderID'=>'',
    			'Amount'=>''
    		);
            
        }
        //Override 父類別函式init_form_fields
        public function init_form_fields(){
            $this->payment_names=array(
                'credit'=>'直接付款',
                'credit_3'=>'分三期'
            );
            //設定頁面的欄位，Override 父類別成員form_fields
            $this->form_fields=array(
                'enabled'=>array(
                    'type'=>'checkbox',
                    'title'=>'啟用金流',
                    'label'=>'啟用',
                    'default'=>'no'
                ),
                'title'=>array(
                    'type'=>'text',
                    'title'=>'金流標題',
                    'default'=>'玉山銀行金流'
                ),
                'description'=>array(
                    'type'=>'text',
                    'title'=>'金流說明',
                    'default'=>'玉山銀行金流刷卡系統'
                ),
                'test_mode' => array(
					'title' => '測試模式',
					'type' => 'checkbox',
					'label' => '啟用',
					'default' => 'yes'
				),
                'merchant_id'=>array(
                    'type'=>'text',
                    'title'=>'店家代號',
                    'default'=>''
                ),
                'terminal_id'=>array(
                    'type'=>'text',
                    'title'=>'端末機代號',
                    'default'=>''
                ),
                'payment_way'=>array(
                    'type'=>'multiselect',
                    'title'=>'付款方式',
                    'options'=>$this->payment_names
                )
            );
        }
        //控制結帳欄位的說明欄位內容，Override 父類別函式payment_fields
        public function payment_fields(){
            echo $this->description;
            $availble_payement_way=$this->get_option('payment_way');
            echo '<select name="payment_way">';
            foreach ($availble_payement_way as $payment_method) {
				echo '<option value="' . $payment_method . '">';
				echo $this->payment_names[$payment_method];
				echo '</option>';
			}
            echo '</select>';
        }
        //控制使用者在按下購買的那一刻，後端會作的事情，Override 父類別函式process_payment
        public function process_payment($order_id){
            $order=new WC_Order($order_id);
            //將選擇分期付款方式存到資料庫
            update_post_meta($order_id,'_choosed_payment',$_POST['payment_way']);
            //更改訂單狀態為等待付款中
            $order->update_status('pending');
			//新增訂單備註
            $order->add_order_note($_POST['payment_way'], true);
            //return值為固定用法
            return array(
                'result'=>'success',
                'redirect'=>$this->get_return_url($order)
            );
        }
        //將自動送出的form，於thank you page顯示出來，以達成自動在付款後導到金流系統
        public function thankyou_page($order_id){
            $order=new WC_Order($order_id);
            //將要送出的資料填入，在設定頁面的資料就用$this->get_option('key')
			echo $order_id.$order->get_total();
        	$this->send['TerminalID']	= $this->get_option('terminal_id');
			
			$this->send['STOREID']	= $this->get_option('merchant_id');
            $this->send['ORDERNUMBER']	= $order_id;
			$this->send['AMOUNT']		= $order->get_total();
			$str=$this->send['STOREID'].$this->send['ORDERNUMBER'].$this->send['AMOUNT'].$this->send['TerminalID'];
			$this->send['CAVALUE']=md5($str);
echo $str;
			//產生form的html
			echo $this->generate_form_html($this->send);
        }
        //產生form的html
        private function generate_form_html($params){
					$mid = $params[STOREID];
					$TerminalID = $params[TerminalID];
					$ORDERNUMBER = $params[ORDERNUMBER];
					//$gwsr = '2012072001';
					$amount = $params[AMOUNT];
					
					$CAVALUE = $params[CAVALUE];

					
					$url=($this->get_option('test_mode')=='yes')?$this->TEST_HOST:$this->HOST;
					$html="<html>";
					$html.="<body>";
					$html.='<form action="'.$url.'" id="_auto_posted_form" method="post" target="_self">';
			 
 
				$data= array();
				$data[MID]=$mid ;
				$data[TID]="EC000001";
				$data[ONO]=$ORDERNUMBER ;
				$data[TA]=$amount;
				$data[U]="http://eosbag.com/wc-api/notify_return";
				$mackey=$TerminalID;
				$mac=hash('sha256',json_encode($data).$mackey);
				$datas=json_encode($data);
				$datas=htmlspecialchars($datas);
					 
        		$html.='<input type="hidden" name="data" value="'.$datas.'">';
				$html.='<input type="hidden" name="mac" value="'.$mac.'">';
				$html.='<input type="hidden" name="ksn" value="1">';
        	//$html.='<input type=submit name="button" value="授權">';
        	$html.='</form>';
		

        	//加入自動送出的script
        	$html.="<script type='text/javascript'>document.getElementById('_auto_posted_form').submit();</script>";
        	$html.="</body>";
			$html.="</html>";
			return $html;
        }
    }
?>
